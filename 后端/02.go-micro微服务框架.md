# 学习使用Go-Micro框架
github link: https://github.com/micro/micro

### protobuf & grpc
原则上，grpc是一种语言无关的远程调用框架，该框架对于多种语言都有提供支持，如：Java, Golang, Python, C/C++, Php，因此无论使用何种语言，都可以应用该框架进行服务实现。但是micro这个框架目前似乎只支持golang，所以在学习时，优先学习golang的部分。
* 官网（被墙）：https://grpc.io

### 微服务
网上对于微服务的概念有很多讲解，总的来说，这是一种分布式服务结构，但对于服务的模块化有更高的要求。

### go-micro微服务框架
* github link: https://github.com/micro/micro
